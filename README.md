The CERN Benchmark suite is a tool which aggregates several different benchmarks in one single application.

It is built to comply with several different use cases by allowing the users to specify which benchmarks to run.

At the moment, the available benchmarks on the suite are:
 * LHCb Fast Benchmark
 * ATLAS Kit Validation
 * Whetstone (from the UnixBench benchmark suite)


It is a standalone application that expects the user to pass a list of the benchmarks to be executed (together with the other possible arguments referred in [How to run](./HowToRun.md)).


### Execution modes

#### Online vs Offline
The user has the choice, at launch time, to have the benchmarks results uniquely printed
in the terminal at the end of the execution. This is the _Offline_ mode and must be specified. By default, *the suite will send the results to ElastiSearch*<sup>1</sup> at the end of the execution.



#### With vs without Docker
Installing this package (cern-benchmark-docker) will allow the user to perform the
same actions provided in _cern-benchmark_, but with the Docker container isolation.
When running the application with Docker, due to Docker itself and the eventual need to setup CVMFS, running as a regular _user_ is not possible. On the bright side the usage of Docker decouples the benchmarks execution and dependencies resolving, which might be attractive for some use cases where running the benchmark as _root_ is not a problem.

**NOTE**: when running as a _user_, please make sure all the dependencies and requirements
have already been setup.
