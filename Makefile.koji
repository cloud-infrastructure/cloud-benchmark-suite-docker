SPECFILE=cern-benchmark-docker.spec
NAME=$(shell grep Name: $(SPECFILE) | sed 's/^[^:]*: //' )
PKGVERSION=$(shell grep Version: $(SPECFILE) | sed 's/^[^:]*: //' )
PKGRELEASE=$(shell grep Release: $(SPECFILE) | sed 's/^[^:]*: //' )
PKGNAME=${NAME}-${PKGVERSION}

TARFILE=$(PKGNAME).tar.gz

FILES=cern-benchmark-docker LICENSE README.md Makefile

BUILD=$(shell pwd)/build
PWD=$(shell pwd)

RPMDEFINES_SRC=--define='_topdir $(BUILD)' \
	--define='_sourcedir %{_topdir}/SOURCES/' \
	--define='_builddir %{_topdir}/BUILD' \
	--define='_srcrpmdir %{_topdir}/SOURCES/' \
	--define='_rpmdir %{_topdir}/RPMS/'

RPMDEFINES_BIN=--define='_topdir $(BUILD)' \
	--define='_sourcedir %{_topdir}/SOURCES' \
	--define='_builddir %{_topdir}/BUILD' \
	--define='_srcrpmdir %{_topdir}/SOURCES/' \
	--define='_rpmdir %{_topdir}/RPMS/'

all:
	mkdir -p $(BUILD)
	mkdir -p $(PKGNAME)
	cp -fr $(FILES) $(PKGNAME)
	tar cvzf $(TARFILE) $(PKGNAME) --exclude="*.pyc"
	rm -fr $(PKGNAME)

prepare: all
	@mkdir -p  $(BUILD)/{BUILD,RPMS,SRPMS,SPECS,SOURCES}
	mv $(TARFILE) $(BUILD)/SOURCES

rpm: prepare srpm
	/usr/bin/rpmbuild -bb $(RPMDEFINES_BIN) $(SPECFILE)

srpm: prepare
	/usr/bin/rpmbuild -bs $(RPMDEFINES_SRC) $(SPECFILE)

build:
	koji build ai6 git+ssh://git@gitlab.cern.ch:7999/cloud-infrastructure/cloud-benchmark-suite-docker.git#$(shell git describe --always)

promote:
	koji tag-build ai6-stable $(PKGNAME)-$(PKGRELEASE)

scratch:
	koji build ai6 --scratch --wait git+ssh://git@gitlab.cern.ch:7999/cloud-infrastructure/cloud-benchmark-suite-docker.git#$(shell git describe --always)

clean:
	rm -fr $(BUILD) $(TARFILE) $(PKGNAME)
