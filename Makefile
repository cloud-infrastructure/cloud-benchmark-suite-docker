SHELL=/bin/sh

ROOTDIR?=""

BMKDIR=$(ROOTDIR)/opt/cern-benchmark

MAIN=cern-benchmark-docker
SOFTLINK=/usr/bin/cern-benchmark-docker

OTHERFILES=README.md Makefile

DIR=$(shell pwd)

all: clean prepare install

prepare:
	@echo -e "\n -- Place cern-benchmark-docker at $(BMKDIR) -- \n"

	mkdir -p $(BMKDIR)

	cp -f $(MAIN) $(BMKDIR) ; \
	cp -f README.md $(BMKDIR)/README-docker.md ;\
	cp -f Makefile $(BMKDIR)/Makefile-docker ;\
	chmod a+x $(BMKDIR)/$(MAIN)	; \
	if type cern-benchmark; then \
		chmod a+x $(BMKDIR)/$(MAIN)	; \
		if test ! -e $(SOFTLINK); then \
			ln -s $(BMKDIR)/$(MAIN) $(SOFTLINK) ;\
		fi ;\
 	else \
		echo "WARN: cern-benchmark does not exist. Please make sure you have cern-benchmark installed" ; \
	fi

install:
	@echo -e "\n -- Install and configure default dependencies for docker mode, from $(BMKDIR)/lib... -- \n"

	bash -i -c "source $(BMKDIR)/lib/dependencies.sh; DOCKER_MODE=1; DOCKER_IMAGE_KV=ccordeiro/centos-kv; \
 			DOCKER_IMAGE_PROFILER=ccordeiro/centos-profiler; base_dependencies; kv_dependencies;"	;\


clean:
	@echo -e "\n -- Deleting $(MAIN) -- \n"

	@if test -e $(BMKDIR)/$(MAIN); then \
		rm -fr $(BMKDIR)/$(MAIN) ; \
	fi

	@if test -e $(SOFTLINK); then \
		rm -f $(SOFTLINK) ; \
	fi
