Name: cern-benchmark-docker
Version: 0.0
Release: 13
Summary: CERN Benchmark Suite - Docker mode
Requires: cern-benchmark, docker-engine
Group: Application/System
License: GPL

BuildRoot: %{_tmppath}/%{name}-buildroot
BuildArch: noarch
Prefix: %{_prefix}
Vendor: Cristovao Cordeiro <cristovao.cordeiro@cern.ch>
Source0: %{name}-%{version}.tar.gz

%define _binaries_in_noarch_packages_terminate_build   0

%description
An extra of the CERN Benchmark suite whereby the user can execute all the same
respective benchmarks, but using Docker containers to decouple the benchmark from
the host system.

%prep
%setup -q

%build

%install
make prepare ROOTDIR="$RPM_BUILD_ROOT"

%post
test -e /usr/bin/cern-benchmark-docker || ln -s /opt/cern-benchmark/cern-benchmark /usr/bin/cern-benchmark-docker

%clean
rm -rf $RPM_BUILD_ROOT

%files
/opt/cern-benchmark/*

%changelog
* Thu Apr 14 2016 Cristóvão Cordeiro <cristovao.cordeiro@cern.ch>
- First release. Extra docker mode for cern-benchmark package
